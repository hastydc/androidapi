package com.keepermate.end.point.demo.controller;

import com.keepermate.end.point.demo.domain.Note;
import com.keepermate.end.point.demo.repository.NoteRepository;
import com.keepermate.end.point.demo.vo.NoteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/note")
public class NoteController {
    @Autowired
    NoteRepository noteRepository;

    @GetMapping("/find-all")
    public ResponseEntity findAll() {
        List<Note> noteList = noteRepository.findAll();

        return new ResponseEntity(noteList, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity create(@RequestBody NoteVO noteVO) {
        Note note = new Note();
        note.setNote(noteVO.getNote());
        noteRepository.save(note);

        return new ResponseEntity(note, HttpStatus.OK);
    }
}
