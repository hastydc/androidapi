package com.keepermate.end.point.demo.vo;

public class NoteVO {
    String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
