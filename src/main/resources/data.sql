
DROP TABLE IF EXISTS note;

CREATE TABLE note (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  note VARCHAR(250) NOT NULL
);

